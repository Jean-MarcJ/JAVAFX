public class AlienNiveau2 {    
    private double posX, posY;
    private String color;
    
    
    public AlienNiveau2(double posX, double posY, String color) {
        this.posX = posX;
        this.posY = posY;  
        this.color = color;
    }

    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines ensembleChaines = new EnsembleChaines();
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+3,"     :-''''-:     ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+2,"   .-' ____  '-.  ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+1," ( (  (_()_)  ) ) ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+1,"  `-.   ^^   .-'  ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+1,"     `._==_.'     ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+1,"      __)(___     ",this.color);

        if ((int) (posX % 2) == 0) {
            // Remplacer les chaînes de caractères actuelles par de nouvelles chaînes avec une autre couleur
            EnsembleChaines newEnsemble = new EnsembleChaines();
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+3," !_!_!          :-''''-:         !_!_! ",this.color);
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+2," |___|      .-' ^/^__^/^ '-      |___| ",this.color);
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+1," |   |     ( (   (_()_)  ) )     |   | ",this.color);
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+1," |   |      `-.    ^^   .-'      |   | ",this.color);
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+1," |   |     __   `._==_.'   __    |   | ",this.color);
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+1," |   |____|  |____)!!(____|  |___|   | ",this.color);
            newEnsemble.ajouteChaine((int) this.posX, (int) this.posY+1," |________________)!!(_______________| ",this.color);    
            return newEnsemble;                
        }
        return ensembleChaines;
    }


    public void evolue(int tours,boolean aGauche) {
        if (aGauche) {
            if((int) tours%100==0){
                this.posY-= 8;
            }
            this.posX -= 5;
        }else {
            if((int)tours%100==0){
                this.posY -= 8;
            }
            this.posX += 5;
        } 
    }


    public int getPositionX() {
        return (int) this.posX;
    }

    public int getPositionY() {
        return (int) this.posY;
    }
    
    public boolean contient(int x, int y){
        if(x >= getPositionX() && x <= getPositionX() + 10 && y >= getPositionY() && y <= getPositionY() + 5) {
            return true;
        }
        return false;
    }

    
    public double positionCanonAlien(){
        return this.posX;
    }
        
}




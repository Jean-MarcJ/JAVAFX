public class Projectile {
    private double posX, posY;
    private String color;

    public Projectile(double posX, double posY, String color){
        this.posX = posX;
        this.posY = posY;
        this.color = color;
    }

    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine((int) posX, (int) posY,"°",this.color);
        return chaines;
    }

    public void evolue(){
        this.posY += 0.2;
    }

    public int getPositionX() {
        return (int) this.posX;
    }

    public int getPositionY() {
        return (int) this.posY;
    }
}

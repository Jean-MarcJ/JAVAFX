import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.text.Font;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;




public class Executable extends Application {
    private Pane root;
    private Group caracteres;
    private GestionJeu gestionnaire;
    private int hauteurTexte;
    private int largeurCaractere;
    private Scene sceneRegle;
    private Niveau2 niveau2;
    private Group caracteresNiveau2;
    private int hauteurTexteNiveau2;
    private int largeurCaractereNiveau2;

    public static void main(String[] args) {
        launch(args);
    }

    private void afficherCaracteres(){
        caracteres.getChildren().clear();
        int hauteur = (int) root.getHeight();
        Text score = (Text) root.lookup("#score");
        try {
            score.setText("Score: " + gestionnaire.getScore().getScore());
        } catch (ExceptionScore e) {
            System.out.println("le score est null");
        }
        for( ChainePositionnee c : gestionnaire.getChaines().chaines)
        {
            Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            caracteres.getChildren().add(t);
        }
    }

    public void afficherCaracteresNiveau2(){
        
        caracteresNiveau2.getChildren().clear();
        int hauteur = (int) root.getHeight();
        Text score2 = (Text) root.lookup("#score");
        try{
        score2.setText("Score: " + niveau2.getScore().getScore());
        }catch(ExceptionScore e){
            System.out.println("le score est null");
        }
        for( ChainePositionnee c : niveau2.getChaines().chaines)
        {
            Text t = new Text (c.x*largeurCaractereNiveau2, hauteur - c.y*hauteurTexteNiveau2, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            caracteresNiveau2.getChildren().add(t);
        }
    }

    private void lancerAnimation(boolean lancer) {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            gestionnaire.jouerUnTour();
                            afficherCaracteres();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        if(lancer){
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        }else
            timeline.stop();       
    }

    private void lancerAnimation2() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            niveau2.jouerUnTour();
                            afficherCaracteresNiveau2();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
            
    }
  
    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("IUTO Space Invader");
        caracteres = new Group();
        root= new AnchorPane(caracteres);
        //root.setStyle("-fx-background-color: black;");
        gestionnaire = new GestionJeu();
        Text t=new Text("█");
        t.setFont(Font.font("Monospaced",10));
        hauteurTexte =(int) t.getLayoutBounds().getHeight();
        largeurCaractere = (int) t.getLayoutBounds().getWidth();
             


        Scene sceneJeu = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);

        Button button = new Button("Voir les règles");
        button.setOnAction(e -> {
            primaryStage.setScene(sceneRegle);
        });

        Image imageTitre = new Image("file:img/spaceInvaders.png");
        ImageView imageView = new ImageView(imageTitre);
        imageView.setTranslateY(-30);
       
        Image imageBackground = new Image("file:img/background.jpg");
        BackgroundImage backgroundImage = new BackgroundImage(
            imageBackground,
            BackgroundRepeat.REPEAT,
            BackgroundRepeat.REPEAT,
            BackgroundPosition.CENTER,
            //new BackgroundSize(60.0, 100.0, true, true, true, true)
            BackgroundSize.DEFAULT
        );
        Background background = new Background(backgroundImage);
        
        // image au début
        VBox layout= new VBox(imageView);
        layout.getChildren().addAll(button);
        //layout.setStyle("-fx-background-color: black;");
        layout.setBackground(background);
        layout.setAlignment(Pos.CENTER);
        Scene sceneIntro = new Scene(layout, gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);

        

        
        
        caracteresNiveau2 = new Group();
        root = new AnchorPane(caracteresNiveau2);
        //root.setStyle("-fx-background-color: black;");
        niveau2 = new Niveau2();
        Text text =new Text("█");
        text.setFont(Font.font("Monospaced",10));
        hauteurTexteNiveau2 =(int) text.getLayoutBounds().getHeight();
        largeurCaractereNiveau2 = (int) text.getLayoutBounds().getWidth();
        Scene sceneNiveau2 = new Scene(root,gestionnaire.getLargeur()*largeurCaractereNiveau2,gestionnaire.getHauteur()*hauteurTexteNiveau2);


        VBox infoNiveau1 = new VBox();
        Text texteNiveau =new Text("Surviver a la vague de Monstres");
        Text textLancement =new Text("Appuyer sur entrer lancer le jeu");
        Text textInfo =new Text("Tuer les Aliens du Niveau 1 rapporte 10 points");
        infoNiveau1.setStyle("-fx-background-color: black;");


        textLancement.setFont(Font.font("Monospaced", 20));
        texteNiveau.setFont(Font.font("Monospaced",25));
        textInfo.setFont(Font.font("Monospaced",25));
        textLancement.setFill(Color.GREEN);
        texteNiveau.setFill(Color.GREEN);
        textInfo.setFill(Color.GREEN);
        

        VBox vboxtexte = new VBox(textLancement);
        HBox hboxTexte = new HBox(texteNiveau);
        VBox vboxTexteInfo = new VBox(textInfo);


        vboxtexte.setAlignment(Pos.TOP_CENTER);
        hboxTexte.setAlignment(Pos.CENTER);
        vboxTexteInfo.setAlignment(Pos.TOP_CENTER);

        int hauteurTexteTitre =(int) texteNiveau.getLayoutBounds().getHeight();
        int largeurCaractereTitre = (int) t.getLayoutBounds().getWidth();
        infoNiveau1.getChildren().add(texteNiveau);
        infoNiveau1.getChildren().add(textLancement);
        infoNiveau1.getChildren().add(vboxTexteInfo);
        infoNiveau1.setAlignment(Pos.CENTER);

        vboxtexte.setPadding(new Insets(50));
        hboxTexte.setPadding(new Insets(50));
        vboxTexteInfo.setPadding(new Insets(50));

        sceneRegle = new Scene(infoNiveau1, gestionnaire.getLargeur()*largeurCaractereTitre,gestionnaire.getHauteur()*hauteurTexteTitre);

        sceneRegle.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
            if(key.getCode() == KeyCode.ENTER){
                primaryStage.setScene(sceneJeu);
                this.gestionnaire = new GestionJeu();
                lancerAnimation(true);
            }
        });
        

        GameOver gameOver = new GameOver("0XFF0000");

        VBox vboxGameOver = new VBox();
        TextFlow texteNiveauOver = new TextFlow(new javafx.scene.text.Text(gameOver.getEnsembleChaines()));
        vboxGameOver.getChildren().add(texteNiveauOver);

        Scene sceneGameOVER = new Scene(vboxGameOver, gameOver.getLargeur() * 10, gameOver.getHauteur() * 10);
        
        

        sceneJeu.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
            if(key.getCode()==KeyCode.LEFT)
                gestionnaire.toucheGauche();
            if(key.getCode()==KeyCode.RIGHT)
                gestionnaire.toucheDroite();
            if(key.getCode()==KeyCode.SPACE){
                gestionnaire.toucheEspace();
            }
            if(key.getCode()==KeyCode.ESCAPE){
                primaryStage.setScene(sceneIntro);

            }
            if(gestionnaire.estTermine()){
                primaryStage.setScene(sceneNiveau2);
                this.niveau2 = new Niveau2();
                lancerAnimation2();
            }
        });

        primaryStage.setScene(sceneIntro);
        primaryStage.setResizable(false);
        primaryStage.show();
        try {
            Text score;
            score = new Text("Score: " + gestionnaire.getScore().getScore());
            score.setId("score");
            score.setFont(Font.font("Monospaced", 20));
            score.setFill(Color.GREEN);
            AnchorPane.setTopAnchor(score, 10.0);
            AnchorPane.setLeftAnchor(score, 10.0);
            root.getChildren().add(score);
        } catch (ExceptionScore e1) {
            System.out.println("n'affiche pas le score");
    
        }
    }
}


public class Alien {    
    private double posX, posY;
    private String color;
    
    
    public Alien(double posX, double posY, String color) {
        this.posX = posX;
        this.posY = posY;  
        this.color = color; 
    }

    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines ensembleChaines = new EnsembleChaines();
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+5,"   ▄██▄    ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+4," ▄██████▄  ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+3,"███▄██▄███ ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+2,"  ▄▀▄▄▀▄   ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+1," ▀ ▀  ▀ ▀  ",this.color);
        if ((int) (posX % 4) == 0) {
            // Remplacer les chaînes de caractères actuelles par de nouvelles chaînes avec une autre couleur
            EnsembleChaines newEnsemble = new EnsembleChaines();
            newEnsemble.ajouteChaine((int) posX, (int) posY + 1,"███▄██▄███ ",this.color);
            newEnsemble.ajouteChaine((int) posX, (int) posY,    " ▀ ▀  ▀ ▀  ",this.color);
            return newEnsemble;                
        }
        return ensembleChaines;
    }

    public void evolue(int tours,boolean aGauche) {
        if (aGauche) {
            if((int) tours%100==0){
                this.posY-= 3;
            }
            this.posX -= 0.1;
        }else {
            if((int)tours%100==0){
                this.posY -= 3;
            }
            this.posX += 0.1;
        } 
    }
    public int getPositionX() {
        return (int) this.posX;
    }
    public int getPositionY() {
        return (int) this.posY;
    }
    
    public boolean contient(int x, int y){
        if(x >= getPositionX() && x <= getPositionX() + 10 && y >= getPositionY() && y <= getPositionY() + 5) {
            return true;
        }
        return false;
    }
    
    
    public double positionCanonAlien(){
        return this.posX;
    }
        
}




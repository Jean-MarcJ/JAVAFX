import java.util.ArrayList;
import java.util.List;


public class GestionJeu {
    private int largeur , hauteur;
    private int posX = 0, posY = 30;  
    private EnsembleChaines chaines;
    private Vaisseau vaisseau;
    private List<Projectile> projectile;
    private Score score;
    private List<Alien> aliens;
    private int toursRestants;
    private boolean descendre = false;
    
    private List<ProjectileAlien> projectileAlien;
    private Niveau2 niveauEnCours;
    private List<AlienNiveau2> alienNiveau2;

    public GestionJeu() {
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(posX,"0X0000FF");
        this.projectile = new ArrayList<>();
        this.toursRestants = 100;
        this.hauteur = 60;
        this.largeur = 100;
        this.projectileAlien = new ArrayList<>();
        this.score = new Score();
        
        this.aliens = new ArrayList<>();

        this.aliens.add(new Alien(10,50, "0XFFFFFF"));
        //this.aliens.add(new Alien(25,50)); 
        //this.aliens.add(new Alien(40,50)); 
        //this.aliens.add(new Alien(55,50));
        //this.aliens.add(new Alien(50,50)); 
        //this.aliens.add(new Alien(60,50)); 
        //this.aliens.add(new Alien(70,50));
        //this.aliens.add(new Alien(80,50)); 
        //this.aliens.add(new Alien(90,50)); 

        this.alienNiveau2 = new ArrayList<>();
        

        
        
        this.chaines.union(this.vaisseau.getEnsembleChaines());
        for(Projectile projectile: this.projectile){
            this.chaines.union(projectile.getEnsembleChaines());
        }
        
        for(Alien alien : this.aliens){
            this.projectileAlien.add(new ProjectileAlien(alien.positionCanonAlien(), alien.getPositionY(), "0XFF0000"));
            this.chaines.union(alien.getEnsembleChaines());
        for(ProjectileAlien projectileAlien : this.projectileAlien){
            this.chaines.union(projectileAlien.getEnsembleChaines());
        }
        }    
    }
    
    public int getHauteur(){
        return this.hauteur;
    }

    public int getLargeur(){
        return this.largeur;
    }

    public void toucheGauche(){
        this.posX --;
        this.vaisseau.deplace(this.posX);
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.posX, "0X0000FF");
        this.chaines.union(this.vaisseau.getEnsembleChaines());  
    }

       
    public void toucheDroite(){
        this.posX += 1;
        this.vaisseau.deplace(this.posX);
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.posX, "0X0000FF");
        this.chaines.union(this.vaisseau.getEnsembleChaines());       
        
    }
        
    public void toucheEspace(){
        System.out.println("Appui sur la touche espace");
        Son.tireAlien();
        this.projectile.add(new Projectile(vaisseau.positionCanon(), 0, "0XFFFF00"));
        for(Projectile projectile : this.projectile){
            projectile.evolue();

        }
    }

    public EnsembleChaines getChaines(){
        return this.chaines;
    }   
    
   

    public void toucheAlien(List<Projectile> projectiles) {
        List<Projectile> balleTouches = new ArrayList<>();
        List<Alien> alienTouches = new ArrayList<>();
        for (Projectile projectile : projectiles) {
            for (Alien alien : this.aliens) {
                if (alien.contient(projectile.getPositionX(), projectile.getPositionY())) {
                    balleTouches.add(projectile);
                    alienTouches.add(alien);
                    System.out.println("Un alien a etait elimine");
                }
            }
        } 
        for (Projectile projectile : balleTouches) {
            projectiles.remove(projectile);
        }
        for (Alien alien : alienTouches) {
            this.aliens.remove(alien);
            Son.mortAlien();
            this.score.ajoute(10);
        }
    }

    public Score getScore() throws ExceptionScore{
        return this.score;
    }

    public void tireAlien(List<ProjectileAlien> projectileAliens){
        for(ProjectileAlien projectileAlien : this.projectileAlien ){
            //this.projectileAlien.add(new ProjectileAlien(alien.positionCanonAlien(), alien.getPositionY(), "0XFF0000"));
            projectileAlien.evolue();
    }
        //List<ProjectileAlien> balleTouches = new ArrayList<>();
        //List<Vaisseau> vaisseaus = new ArrayList<>();
        //vaisseaus.add(this.vaisseau);
        //List<Vaisseau> vaisseauTouche = new ArrayList<>();
        //for (ProjectileAlien balleAlien : projectileAliens) {
        //    for (Vaisseau vaisseau : vaisseaus) {
        //        if (vaisseau.touche(balleAlien.getPositionX(), balleAlien.getPositionY())) {
        //            balleTouches.add(balleAlien);
        //            vaisseauTouche.add(vaisseau);
        //            System.out.println("Un alien a était éliminé");
        //        }
        //    }
        //} 
        //for (ProjectileAlien projectile : balleTouches) {
        //    projectileAliens.remove(projectile);
        //}
        //for (Vaisseau vaisseau : vaisseauTouche) {
        //    vaisseaus.remove(vaisseau);
        //    System.out.println("Vous avez perdu");
        //}
    }
//
    //public void videBalle(){
    //    for(ProjectileAlien balleAlien : this.projectileAlien){
    //        if((int)this.toursRestants%4 == 0){
    //            balleAlien.evolue();
    //        }
    //    }
    //}

    public void jouerUnTour(){
        this.chaines = this.vaisseau.getEnsembleChaines();

        
        for(Projectile projectile : this.projectile){
            projectile.evolue();
            this.chaines.union(projectile.getEnsembleChaines());
        }

        for(Alien alien : this.aliens){
            alien.evolue(this.toursRestants, descendre);
            this.chaines.union(alien.getEnsembleChaines());
            this.tireAlien(this.projectileAlien);
        }

        for(AlienNiveau2 alien : this.alienNiveau2 ){
            alien.evolue(this.toursRestants, descendre);
            this.chaines.union(alien.getEnsembleChaines());
            this.tireAlien(this.projectileAlien);
        }

        //for(ProjectileAlien balleAlien : this.projectileAlien){
        //    balleAlien.evolue();
        //    this.chaines.union(balleAlien.getEnsembleChaines());
        //}

        if((int)this.toursRestants%100 == 0){
            if(descendre){
                descendre = false;
            }
            else{
                descendre = true;
            }
        }

        this.toursRestants += 1;
        this.toucheAlien(this.projectile);       

        //if (aliens.isEmpty()) {
        //    // Passer au niveau 2
        //    niveauEnCours = new Niveau2();
        //    // Réinitialiser les vies et le score du joueur
        //    niveauEnCours.getEnsembleChaines();
        //}

    }


    public boolean estTermine(){
        if(this.aliens.isEmpty()){
            return true;
        }
        return false;
    }

    
    
}
import java.util.ArrayList;
import java.util.List;


public class Niveau2 extends GestionJeu {
    private int largeur , hauteur;
    private int posX = 0, posY = 30;  
    private EnsembleChaines chaines;
    private Vaisseau vaisseau;
    private List<Projectile> projectile;
    private Score score;
    private List<Alien2> aliensNiveau2;
    private int toursRestants;
    private boolean descendre = false;
    private List<Alien> alienTouches;    
    private List<ProjectileAlien> projectileAlien;

    public Niveau2() {
        this.toursRestants = 100;
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(posX,"0X0000FF");
        this.projectile = new ArrayList<>();
        this.hauteur = 60;
        this.largeur = 100;
        this.projectileAlien = new ArrayList<>();
        this.score = new Score();
        
        this.chaines.union(this.vaisseau.getEnsembleChaines());

        this.aliensNiveau2 = new ArrayList<>();
        this.aliensNiveau2.add(new Alien2(10,50, "0XFFFFFF"));
        

        
        for(Alien2 alien : this.aliensNiveau2){
            this.projectileAlien.add(new ProjectileAlien(alien.positionCanonAlien(), alien.getPositionY(), "0XFF0000"));
            this.chaines.union(alien.getEnsembleChaines());
        }

        for(Projectile projectile: this.projectile){
            this.chaines.union(projectile.getEnsembleChaines());
        }        
    }

    public EnsembleChaines getEnsembleChaines() {
        // Récupération des chaînes de caractères de l'alien
        return this.chaines;
    }
    

    public void toucheGauche(){
        this.posX --;
        this.vaisseau.deplace(this.posX);
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.posX, "0X0000FF");
        this.chaines.union(this.vaisseau.getEnsembleChaines());  
    }

       
    public void toucheDroite(){
        this.posX += 1;
        this.vaisseau.deplace(this.posX);
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.posX, "0X0000FF");
        this.chaines.union(this.vaisseau.getEnsembleChaines());       
        
    }
        
    public void toucheEspace(){
        System.out.println("Appui sur la touche espace");
        Son.tireAlien();
        this.projectile.add(new Projectile(vaisseau.positionCanon(), 0, "0XFFFF00"));
        for(Projectile projectile : this.projectile){
            projectile.evolue();

        }
    }

    public int getHauteur(){
        return this.hauteur;
    }

    public int getLargeur(){
        return this.largeur;
    }
    
    public void toucheAlien(List<Projectile> projectiles) {
        List<Projectile> balleTouches = new ArrayList<>();
        List<Alien2> alienTouches = new ArrayList<>();
        for (Projectile projectile : projectiles) {
            for (Alien2 alien : this.aliensNiveau2) {
                if (alien.contient(projectile.getPositionX(), projectile.getPositionY())) {
                    balleTouches.add(projectile);
                    alienTouches.add(alien);
                    System.out.println("Un alien a etait elimine");
                }
            }
        } 
        for (Projectile projectile : balleTouches) {
            projectiles.remove(projectile);
        }
        for (Alien2 alien : alienTouches) {
            this.aliensNiveau2.remove(alien);
            Son.mortAlien();
            this.score.ajoute(30);
        }
    }   

    public Score getScore() throws ExceptionScore{ 
        return this.score;
    }


    public void jouerUnTour(){
        this.chaines = this.vaisseau.getEnsembleChaines();

        
        for(Projectile projectile : this.projectile){
            projectile.evolue();
            this.chaines.union(projectile.getEnsembleChaines());
        }

        for(Alien2 alien : this.aliensNiveau2 ){
            //alien.evolue(this.toursRestants, descendre);
            this.chaines.union(alien.getEnsembleChaines());
            this.tireAlien(this.projectileAlien);
        }

        if((int)this.toursRestants%100 == 0){
            if(descendre){
                descendre = false;
            }
            else{
                descendre = true;
            }
        }

        this.toursRestants += 1;
        this.toucheAlien(this.projectile); 
    }

}
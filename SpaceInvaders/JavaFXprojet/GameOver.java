import javafx.scene.text.Text;

public class GameOver {
    private double posX = 15, posY =30;
    private String color;
    private int largeur , hauteur;
    
    public GameOver(String color){
        this.color = color;
        this.hauteur = 60;
        this.largeur = 100;
    }

    public int getHauteur(){
        return this.hauteur;
    }

    public int getLargeur(){
        return this.largeur;
    }

    public String getEnsembleChaines() {
        return "      GGGGGGGGGGGG              AA             MMMM            MMMM      EEEEEEEEEEEEEEEEEEEE          OOOOOOO         VVVVVVVV           VVVVVVVV    EEEEEEEEEEEEEEEEEEE    RRRRRRRRRRRRRRRRR      \n" +
               "    GGGGGGGGGGGGGGGG           AAAA            MMMM MMM    MMM MMMM      EEEEEEEEEEEEEEEEEEEE        OOOOOOOOOOOO       VVVVVVVV         VVVVVVVV     EEEEEEEEEEEEEEEEEEE    RRRRRRRRRRRRRRRRR      \n" +
               "   GGG           GGG          AA  AA           MMMM     MMM    MMMM      EEEE                      OOOOO      OOOOO      VVVVVVVV       VVVVVVVV      EEEE                   RRRR         RRRR      \n" +
               "  GGG                        AA    AA          MMMM      M     MMMM      EEEE                      OOOO        OOOO       VVVVVVVV     VVVVVVVV       EEEE                   RRRR         RRRR      \n" +
               "  GGG                       AAAAAAAAAA         MMMM            MMMM      EEEEEEEEEEEEEEEEEEEE      OOOO        OOOO        VVVVVVVV   VVVVVVVV        EEEEEEEEEEEEEEEEE      RRRRRRRRRRRRRRRR       \n" +
               "  GGG        GGGGG         AAAAAAAAAAAA        MMMM            MMMM      EEEEEEEEEEEEEEEEEEEE      OOOO        OOOO         VVVVVVVV VVVVVVVV         EEEEEEEEEEEEEEEEE      RRRRRRRRRRRRRRRR       \n" +
               "  GGG           GGG       AAA        AAA       MMMM            MMMM      EEEE                      OOOO        OOOO          VVVVVVVVVVVVVVV          EEEE                   RRRR      RRRR         \n" +
               "   GGG           GGG     AAA          AAA      MMMM            MMMM      EEEE                      OOOOO      OOOOO            VVVVVVVVVVVV           EEEE                   RRRR       RRRR        \n" +
               "    GGGGGGGGGGGGGGGG    AAA            AAA     MMMM            MMMM      EEEEEEEEEEEEEEEEEEEE        OOOOOOOOOOOO               VVVVVVVVVV            EEEEEEEEEEEEEEEEEEE    RRRR         RRRR      \n" +
               "      GGGGGGGGGGGG     AAA              AAA    MMMM            MMMM      EEEEEEEEEEEEEEEEEEEE          OOOOOOO                   VVVVVVVV             EEEEEEEEEEEEEEEEEEE    RRRR           RRRR    ";
    }
    
}

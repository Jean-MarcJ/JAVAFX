import java.io.File;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Son {
    
    // Méthode pour jouer le son du tir
    public static void tireAlien() {
        String sonTir = "son/tireVaisseau.mp3"; // Nom du fichier audio
        Media son = new Media(new File(sonTir).toURI().toString()); // Création de l'objet Media
        MediaPlayer mediaPlayer = new MediaPlayer(son); // Création de l'objet MediaPlayer
        mediaPlayer.play(); // Lecture du son
    }

    // Méthode pour jouer l'explosion des Aliens
    public static void mortAlien() {
        String sonTir = "son/mortAlien.mp3"; // Nom du fichier audio
        Media son = new Media(new File(sonTir).toURI().toString()); // Création de l'objet Media
        MediaPlayer mediaPlayer = new MediaPlayer(son); // Création de l'objet MediaPlayer
        mediaPlayer.play(); // Lecture du son
    }

    
}

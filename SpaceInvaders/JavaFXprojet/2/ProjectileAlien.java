
public class ProjectileAlien extends Projectile {
    private double posX, posY;
    private String color;

    public ProjectileAlien(double posX, double posY, String color) {
        super(posX, posY, color);
    }
    
    
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines BalleAlien = new EnsembleChaines();
        BalleAlien.ajouteChaine((int) posX, (int) posY,"  ▀▀▀▀▀ ",this.color);
        BalleAlien.ajouteChaine((int) posX, (int) posY-1," ▀▀▀  ",this.color);
        BalleAlien.ajouteChaine((int) posX, (int) posY-2,"  ▀   ",this.color);
        return BalleAlien;
    }

    
    public void evolue(){
        this.posY --;
    }
}

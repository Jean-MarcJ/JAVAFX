public class ChainePositionnee{
    int x,y;
    String c;
    private String color;

    
    public ChainePositionnee(int a,int b, String d, String couleur){
        x=a; 
        y=b; 
        c=d; 
        this.color = couleur;
    }

    public int tailleChaine(){
        return c.length();
    }

    public String getColor(){
        return this.color;
    }
    
    
} 

   
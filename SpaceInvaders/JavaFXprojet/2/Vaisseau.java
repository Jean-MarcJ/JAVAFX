
    public class Vaisseau {
        private double posX;
        private EnsembleChaines ensembleChaines;
        private String color;

        public Vaisseau(double posX, String color) {
            this.posX = posX;
            this.color = color;
        }

        public void deplace(double dx) {
            this.posX += dx;
        }

        public EnsembleChaines getEnsembleChaines() {
            this.ensembleChaines = new EnsembleChaines();
            this.ensembleChaines.ajouteChaine((int) this.posX, 8, "          ██           ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX, 7, "         ████          ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX, 6, "      ██ ████ ██       ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX, 5, "      ██ ████ ██       ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX,  4," ██   ██████████    ██ ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX,  3," ██ █ ███████████ █ ██ ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX,  2," █████████████████████ ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX,  1,"     ██████████████    ",this.color);
            this.ensembleChaines.ajouteChaine((int) this.posX,  0,"        ██ ██ ██       ",this.color);
            return this.ensembleChaines;

        }

        //public EnsembleChaines removeEnsembleChaines(EnsembleChaines ensembleChaines){
        //    ensembleChaines = new EnsembleChaines();
        //    return ensembleChaines;
        //}

        public double positionCanon() { 
            return this.posX + 11;
        }

        public double getPositionX(){
            return this.posX;
        }

        public boolean touche(int x, int y){
            return getEnsembleChaines().contient(x, y);    
        }

        
    }

public class Score {
    private int score;

    public Score(){
        this.score = 0;
    }

    public void ajoute(int valeur){
        this.score += valeur;
    }

    public int getScore(){
        return this.score;
    }
}

    import javafx.application.Application;
    import javafx.event.EventHandler;
    import javafx.geometry.Insets;
    import javafx.geometry.Pos;
    import javafx.scene.Scene;
    import javafx.scene.control.Button;
    import javafx.scene.image.Image;
    import javafx.scene.image.ImageView;
    import javafx.scene.layout.Pane;
    import javafx.scene.layout.StackPane;
    import javafx.scene.layout.VBox;
    import javafx.scene.paint.Color;
    import javafx.scene.layout.AnchorPane;
    import javafx.scene.layout.Background;
    import javafx.scene.layout.BackgroundImage;
    import javafx.scene.layout.BackgroundPosition;
    import javafx.scene.layout.BackgroundRepeat;
    import javafx.scene.layout.BackgroundSize;
    import javafx.scene.layout.HBox;
    import javafx.stage.Stage;
    import javafx.scene.Group;
    import javafx.scene.text.Text;
    import javafx.scene.text.Font;
    import javafx.scene.text.FontWeight;
    import javafx.animation.Animation;
    import javafx.animation.KeyFrame;
    import javafx.animation.Timeline;
    import javafx.event.ActionEvent;
    import javafx.util.Duration;
    import javafx.scene.input.KeyEvent;
    import javafx.scene.input.KeyCode;




    public class Executable extends Application {
        private Pane root;
        private Group caracteres;
        private GestionJeu gestionnaire;
        private int hauteurTexte;
        private int largeurCaractere;
        private Scene sceneRegle;
        public static void main(String[] args) {
            launch(args);
        }

        private void afficherCaracteres(){
            caracteres.getChildren().clear();
            int hauteur = (int) root.getHeight();
            Text score = (Text) root.lookup("#score");
         
            try {
                score.setText("Score: " + gestionnaire.getScore());
                
            } catch (ExceptionScore e) {
                System.out.println("le score est null");
            }
            for( ChainePositionnee c : gestionnaire.getChaines().chaines)
            {
                Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
                t.setFont(Font.font ("Monospaced", 10));
                caracteres.getChildren().add(t);
            }
        }

    
        private void lancerAnimation(boolean lancer) {
            Timeline timeline = new Timeline(
                    new KeyFrame(Duration.seconds(0),
                        new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent actionEvent) {
                                gestionnaire.jouerUnTour();
                                afficherCaracteres();
                            }
                        }),
                    new KeyFrame(Duration.seconds(0.025))
                    );
            if(lancer){
            timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
            }else
                timeline.stop();       
        }
    
        @Override
        public void start(Stage primaryStage){
            primaryStage.setTitle("IUTO Space Invader");
            caracteres = new Group();
            root= new AnchorPane(caracteres);
            gestionnaire = new GestionJeu();
            Text t=new Text("█");
            t.setFont(Font.font("Monospaced",10));
            hauteurTexte =(int) t.getLayoutBounds().getHeight();
            largeurCaractere = (int) t.getLayoutBounds().getWidth();
            Pane root2 = new AnchorPane(caracteres);
            //Scene sceneNiveau2 = new Scene(root2, gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            StackPane stackPane = new StackPane();
            stackPane.getChildren().addAll(root, root2);

            Scene sceneNiveau1 = new Scene(stackPane,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);

            Button button = new Button("Voir les regles");
            button.setOnAction(e -> {
                primaryStage.setScene(sceneRegle);
            });

            Image imageTitre = new Image("file:img/spaceInvaders.png");
            ImageView imageView = new ImageView(imageTitre);
            imageView.setTranslateY(-30);
        
            Image imageBackground = new Image("file:img/background.jpg");
            BackgroundImage backgroundImage = new BackgroundImage(
                imageBackground,
                BackgroundRepeat.REPEAT,
                BackgroundRepeat.REPEAT,
                BackgroundPosition.CENTER,
                BackgroundSize.DEFAULT
            );
            Background background = new Background(backgroundImage);
            
            // image au début
            VBox layout= new VBox(imageView);
            layout.getChildren().addAll(button);
            //layout.setStyle("-fx-background-color: black;");
            layout.setBackground(background);
            layout.setAlignment(Pos.CENTER);
            Scene sceneIntro = new Scene(layout, gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);

            VBox infoNiveau1 = new VBox();
            Text texteNiveau =new Text("Surviver a la vague de Monstres");
            Text textLancement =new Text("Appuyer sur entrer lancer le jeu");
            Text textInfo =new Text("Tuer les Aliens du Niveau 1 rapporte 10 points");
            infoNiveau1.setStyle("-fx-background-color: black;");


            textLancement.setFont(Font.font("Monospaced", 20));
            texteNiveau.setFont(Font.font("Monospaced",25));
            textInfo.setFont(Font.font("Monospaced",25));
            textLancement.setFill(Color.GREEN);
            texteNiveau.setFill(Color.GREEN);
            textInfo.setFill(Color.GREEN);
            

            VBox vboxtexte = new VBox(textLancement);
            HBox hboxTexte = new HBox(texteNiveau);
            VBox vboxTexteInfo = new VBox(textInfo);


            vboxtexte.setAlignment(Pos.TOP_CENTER);
            hboxTexte.setAlignment(Pos.CENTER);
            vboxTexteInfo.setAlignment(Pos.TOP_CENTER);

            infoNiveau1.getChildren().add(texteNiveau);
            infoNiveau1.getChildren().add(textLancement);
            infoNiveau1.getChildren().add(vboxTexteInfo);
            infoNiveau1.setAlignment(Pos.CENTER);

            vboxtexte.setPadding(new Insets(50));
            hboxTexte.setPadding(new Insets(50));
            vboxTexteInfo.setPadding(new Insets(50));

            sceneRegle = new Scene(infoNiveau1, gestionnaire.getLargeur() * largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);


            VBox infoNiveau2 = new VBox();
            Text texteNiveau2 = new Text("Survivre a la vague de Monstres");
            Text textLancement2 = new Text("Attendre 10 secondes");
            Text textInfo2 = new Text("Tuer les Aliens du Niveau 2 rapporte 30 points");
            infoNiveau2.setStyle("-fx-background-color: black;");
            
            textLancement2.setFont(Font.font("Monospaced", 20));
            texteNiveau2.setFont(Font.font("Monospaced", 25));
            textInfo2.setFont(Font.font("Monospaced", 25));
            textLancement2.setFill(Color.GREEN);
            texteNiveau2.setFill(Color.GREEN);
            textInfo2.setFill(Color.GREEN);
            
            textLancement2.setFont(textLancement.getFont());
            texteNiveau2.setFont(texteNiveau.getFont());
            textInfo2.setFont(textInfo.getFont());
            
            VBox vboxtexte2 = new VBox(textLancement2);
            HBox hboxTexte2 = new HBox(texteNiveau2);
            VBox vboxTexteInfo2 = new VBox(textInfo2);
            
            vboxtexte2.setAlignment(Pos.TOP_CENTER);
            hboxTexte2.setAlignment(Pos.CENTER);
            vboxTexteInfo2.setAlignment(Pos.TOP_CENTER);
            
            infoNiveau2.getChildren().add(texteNiveau2);
            infoNiveau2.getChildren().add(textLancement2);
            infoNiveau2.getChildren().add(vboxTexteInfo2);
            infoNiveau2.setAlignment(Pos.CENTER);
            
            vboxtexte2.setPadding(new Insets(50));
            hboxTexte2.setPadding(new Insets(50));
            vboxTexteInfo2.setPadding(new Insets(50));
            
            Scene sceneRegle2 = new Scene(infoNiveau2, gestionnaire.getLargeur() * largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            
            Text gameOverText = new Text("GAME OVER");
            gameOverText.setFont(Font.font("Arial", FontWeight.BOLD, 80));
            gameOverText.setFill(Color.RED);

            StackPane root3 = new StackPane(gameOverText);
            root3.setAlignment(Pos.CENTER);

            Scene gameOverScene = new Scene(root3, gestionnaire.getLargeur() * largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);

            sceneRegle.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }
                if(key.getCode() == KeyCode.ENTER){
                    primaryStage.setScene(sceneNiveau1);
                    this.gestionnaire = new GestionJeu();
                    this.gestionnaire.Niveau1();
                    this.gestionnaire.jouerUnTour();
                    lancerAnimation(true);
                }
            });   

            sceneRegle2.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }
                if(key.getCode() == KeyCode.ENTER){
                    primaryStage.setScene(sceneNiveau1);
                    this.gestionnaire = new GestionJeu();
                    this.gestionnaire.Niveau2();
                    this.gestionnaire.jouerUnTour();
                    lancerAnimation(true);
                }
                
            });   
            
            
            sceneNiveau1.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                //if(key.getCode() == KeyCode.ENTER){
                //    primaryStage.setScene(sceneNiveau1);
                //    this.gestionnaire = new GestionJeu();
                //    this.gestionnaire.Niveau1();
                //    this.gestionnaire.jouerUnTour();
                //    lancerAnimation(true);
                //}
                if(key.getCode()==KeyCode.LEFT)
                    gestionnaire.toucheGauche();
                if(key.getCode()==KeyCode.RIGHT)
                    gestionnaire.toucheDroite();
                if(key.getCode()==KeyCode.SPACE){
                    gestionnaire.toucheEspace();
                }
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }
                if(gestionnaire.finNiveau())
                    primaryStage.setScene(sceneRegle2);                    
                if(gestionnaire.estPerdu())
                    primaryStage.setScene(gameOverScene);            
            });

            //sceneNiveau2.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
            //    if(key.getCode()==KeyCode.LEFT)
            //        gestionnaire.toucheGauche();
            //    if(key.getCode()==KeyCode.RIGHT)
            //        gestionnaire.toucheDroite();
            //    if(key.getCode()==KeyCode.SPACE){
            //        gestionnaire.toucheEspace();
            //    }
            //    if(key.getCode()==KeyCode.ESCAPE){
            //        primaryStage.setScene(sceneIntro);
            //    }
            //    //if(key.getCode() == KeyCode.ENTER){
            //    //    primaryStage.setScene(sceneNiveau2);
            //    //    this.gestionnaire = new GestionJeu();
            //    //    this.gestionnaire.Niveau2();
            //    //    this.gestionnaire.jouerUnTour();
            //    //    lancerAnimation(true);
            //    //}
            //    if(gestionnaire.estPerdu())
            //        primaryStage.setScene(gameOverScene);  
            //});   

            primaryStage.setScene(sceneIntro);
            primaryStage.setResizable(false);
            primaryStage.show();
            try {
                Text score;
                score = new Text("Score: " + gestionnaire.getScore());
                score.setId("score");
                score.setFont(Font.font("Monospaced", 20));
                score.setFill(Color.GREEN);
                AnchorPane.setTopAnchor(score, 10.0);
                AnchorPane.setLeftAnchor(score, 10.0);
                root.getChildren().add(score);
            } catch (ExceptionScore e1) {
                System.out.println("n'affiche pas le score");
        
            }
        }
    }

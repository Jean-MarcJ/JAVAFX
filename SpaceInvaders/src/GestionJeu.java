import java.util.ArrayList;
import java.util.List;


public class GestionJeu {
    private int largeur , hauteur;
    private int posX = 0, posY = 30;  
    private EnsembleChaines chaines;
    private Vaisseau vaisseau;
    private List<Projectile> projectile;
    private Score score;
    private List<Alien> aliens;
    private int toursRestants;
    private boolean descendre = false;
    private List<Alien2> alienNiveau2;

    /**
    * Initialise une instance de la classe GestionJeu.
    * Cette méthode crée un vaisseau, des projectiles, un score, des aliens de niveau 1 et 2,
    * et met à jour l'ensemble des chaînes de la classe.
    */
    public GestionJeu() {
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(posX,"0X0000FF");
        this.projectile = new ArrayList<>();
        this.toursRestants = 100;
        this.hauteur = 60;
        this.largeur = 100;
        this.score = new Score();
        
        this.aliens = new ArrayList<>();  
        this.aliens.add(new Alien(25,50, "0XFFFFFF"));

        this.alienNiveau2 = new ArrayList<>();


        this.chaines.union(this.vaisseau.getEnsembleChaines());
        for(Projectile projectile: this.projectile){
            this.chaines.union(projectile.getEnsembleChaines());
        }
        
        for(Alien alien : this.aliens){
            this.chaines.union(alien.getEnsembleChaines());
        }    
    }

    
    /**
     * Vérifie si le joueur a perdu la partie.
     *
     * @return vrai si un alien a atteint le bas de l'écran, faux sinon
     */
    public boolean estPerdu(){
        for(Alien alien : this.aliens){
            if(alien.getPositionY() <= this.vaisseau.getPositionY()){
                return true;
            }
        }
        for(Alien2 alien2: this.alienNiveau2){
            if(alien2.getPositionY() <= this.vaisseau.getPositionY()){
                return true;
            }
        }
        return false;
    }

    /**
    * Vérifie si le joueur a terminé le niveau.
    *
    * @return vrai si tous les aliens ont été éliminés, faux sinon
    */
    public boolean finNiveau(){
        if(this.aliens.isEmpty() && this.alienNiveau2.isEmpty()){
            return true;
        }
        return false;
    }
    
    /**
    * Crée des aliens de niveau 1 et 2 et les ajoute à la liste d'aliens de la classe.
    *
    * @return l'ensemble des chaînes de la classe GestionJeu
    */
    public EnsembleChaines Niveau(){
        int posXAlien= 25;
        int posXAlien2= 15;
        int posYAlien = 50;
        this.aliens.add(new Alien(posXAlien + 15,50, "0XFFFFFF"));
        this.aliens.add(new Alien(posXAlien + 30,50, "0XFFFFFF")); 
        this.aliens.add(new Alien(posXAlien + 45,50, "0XFFFFFF"));
        for(int i = 0; i<=3; i++){
            posYAlien += 15;
            this.aliens.add(new Alien(posXAlien,posYAlien, "0XFFFFFF"));
            this.aliens.add(new Alien(posXAlien+15,posYAlien, "0XFFFFFF"));
            this.aliens.add(new Alien(posXAlien +30,posYAlien, "0XFFFFFF"));
            this.aliens.add(new Alien(posXAlien + 45,posYAlien, "0XFFFFFF"));
            for(Alien alien : this.aliens){
                this.chaines.union(alien.getEnsembleChaines());
            }    
        }
        for(int i = 0; i <= 3; i++){
            posYAlien += 10;
            this.alienNiveau2.add(new Alien2(posXAlien2, posYAlien, "0XFF0000"));
            this.alienNiveau2.add(new Alien2(posXAlien2+40, posYAlien, "0XFF0000"));
            for(Alien2 alien : this.alienNiveau2){
                this.chaines.union(alien.getEnsembleChaines());
            }
        }
        return this.chaines;
    }

    /**
    * Retourne la hauteur de l'écran de jeu.
    *
    * @return la hauteur de l'écran de jeu
    */
    public int getHauteur(){
        return this.hauteur;
    }
     
    /**
    * Retourne la largeur de l'écran de jeu.
    *
    * @return la largeur de l'écran de jeu
    */
    public int getLargeur(){
        return this.largeur;
    }

    /**
    * Déplace le vaisseau vers la gauche en mettant à jour sa position et l'ensemble des chaînes.
    */
    public void toucheGauche(){
        this.posX --;
        this.vaisseau.deplace(this.posX);
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.posX, "0X0000FF");
        this.chaines.union(this.vaisseau.getEnsembleChaines());  
    }

    /**
    * Déplace le vaisseau vers la droite en mettant à jour sa position et l'ensemble des chaînes.
    */   
    public void toucheDroite(){
        this.posX += 1;
        this.vaisseau.deplace(this.posX);
        this.chaines = new EnsembleChaines();
        this.vaisseau = new Vaisseau(this.posX, "0X0000FF");
        this.chaines.union(this.vaisseau.getEnsembleChaines());       
        
    }
    
    /**
    * Ajoute un projectile au jeu et le fait évoluer en le mettant à jour.
    */
    public void toucheEspace(){
        System.out.println("Appuie sur la touche espace");
        if(this.projectile.size() <= 2){
        this.projectile.add(new Projectile(vaisseau.positionCanon(), 9, "0XFFFF00"));
        }
        for(Projectile projectile : this.projectile){
            projectile.evolue();

        }
    }
    /**
    * Retourne l'ensemble des chaînes du jeu.
    *
    * @return l'ensemble des chaînes du jeu
    */
    public EnsembleChaines getChaines(){
        return this.chaines;
    }   
    
   
    /**
    * Vérifie si un projectile touche un alien ou un alien de niveau 2.
    * Si c'est le cas, retire le projectile et l'alien touché, met à jour le score.
    *
    * @param projectiles la liste des projectiles tirés par le joueur
    */
    public void toucheAlien(List<Projectile> projectiles) {
        List<Projectile> balleTouches = new ArrayList<>();
        List<Alien> alienTouches = new ArrayList<>();
        List<Alien2> listAlienNiveau2 = new ArrayList<>();
        for (Projectile projectile : projectiles) {
            if(!this.aliens.isEmpty()){
            for (Alien alien : this.aliens) {
                    if (alien.contient(projectile.getPositionX(), projectile.getPositionY())) {
                        balleTouches.add(projectile);
                        alienTouches.add(alien);
                        System.out.println("Un alien a etait elimine");
                    }
                }
            }
            if(!this.alienNiveau2.isEmpty()){
            for(Alien2 alienNiveau2 : this.alienNiveau2){
                    if (alienNiveau2.contient(projectile.getPositionX(), projectile.getPositionY())) {
                        balleTouches.add(projectile);
                        listAlienNiveau2.add(alienNiveau2);
                        System.out.println("Un alien a etait elimine");
                    }
                }
            }
        } 
        for (Projectile projectile : balleTouches) {
            projectiles.remove(projectile);
        }
        for (Alien alien : alienTouches) {
            this.aliens.remove(alien);
            this.score.ajoute(10);
        }
        for (Alien2 alienNiveau2 : listAlienNiveau2) {
            this.alienNiveau2.remove(alienNiveau2);
            this.score.ajoute(30);
        }
    }

    /**
    * Returns the current score of the game.
    *
    * @return the score of the game
    * @throws ExceptionScore if the score is not valid
    */
    public int getScore() throws ExceptionScore{
        return this.score.getScore();
    }


    
    /**
    * Simulates one turn of the game.
    * 
    * Updates the positions of the projectiles and aliens,
    * checks for collisions, and updates the game state accordingly.
    * 
    */
    public void jouerUnTour(){
        this.chaines = this.vaisseau.getEnsembleChaines();

        
        for(Projectile projectile : this.projectile){
            projectile.evolue();
            this.chaines.union(projectile.getEnsembleChaines());
           
        }
        try{
            for(Projectile p : this.projectile){
                if(p.getPositionY() >= getHauteur()){
                    this.projectile.remove(p);
                }
            }
        }
        catch(Exception e){
            this.chaines = this.vaisseau.getEnsembleChaines();

        }

        
        for(Alien alien : this.aliens){
            alien.evolue(this.toursRestants, descendre);
            this.chaines.union(alien.getEnsembleChaines());
        }

        for(Alien2 alien : this.alienNiveau2 ){
            alien.evolue(this.toursRestants, descendre);
            this.chaines.union(alien.getEnsembleChaines());
        }

        if((int)this.toursRestants%100 == 0){
            if(descendre){
                descendre = false;
            }
            else{
                descendre = true;
            }
        }

        this.toursRestants += 1;
        this.toucheAlien(this.projectile);       
        this.estPerdu();
        this.finNiveau();

    }
    
    
}

public class Alien {    
    private double posX, posY;
    private String color;
    
    /**
    * Constructeur de la classe Alien.
    * @param posX la position horizontale de l'alien.
    * @param posY la position verticale de l'alien.
    * @param color la couleur de l'alien.
    */
    public Alien(double posX, double posY, String color) {
        this.posX = posX;
        this.posY = posY;  
        this.color = color; 
    }

    /**
    * Retourne l'ensemble de chaînes représentant l'alien.
    * @return un objet EnsembleChaines contenant les chaînes de caractères représentant l'alien.
    */
    public EnsembleChaines getEnsembleChaines() {
        EnsembleChaines ensembleChaines = new EnsembleChaines();
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+5,"   ▄██▄    ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+4," ▄██████▄  ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+3,"███▄██▄███ ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+2,"  ▄▀▄▄▀▄   ",this.color);
        ensembleChaines.ajouteChaine((int) this.posX, (int) this.posY+1," ▀ ▀  ▀ ▀  ",this.color);
        if ((int) (posX % 4) == 0) {
            // Remplacer les chaînes de caractères actuelles par de nouvelles chaînes avec une autre couleur
            EnsembleChaines newEnsemble = new EnsembleChaines();
            newEnsemble.ajouteChaine((int) posX, (int) posY + 1,"███▄██▄███ ",this.color);
            newEnsemble.ajouteChaine((int) posX, (int) posY,    " ▀ ▀  ▀ ▀  ",this.color);
            return newEnsemble;                
        }
        return ensembleChaines;
    }

     /**
    * Fait évoluer l'alien en modifiant sa position.
    * @param tours le nombre de tours effectués depuis le début du jeu.
    * @param aGauche un booléen indiquant si l'alien doit aller à gauche (true) ou à droite (false).
    */
    public void evolue(int tours,boolean aGauche) {
        if (aGauche) {
            if((int) tours%100==0){
                this.posY-= 3;
            }
            this.posX -= 0.1;
        }else {
            if((int)tours%100==0){
                this.posY -= 3;
            }
            this.posX += 0.1;
        } 
    }

    /**
    * Retourne la position horizontale de l'alien.
    * @return la position horizontale de l'alien.
    */
    public int getPositionX() {
        return (int) this.posX;
    }

    /**
    * Retourne la position verticale de l'alien.
    * @return la position verticale de l'alien.
    */
    public int getPositionY() {
        return (int) this.posY;
    }
    
    
    /**
    * Vérifie si l'alien contient les coordonnées (x, y).
    * @param x la coordonnée horizontale à tester.
    * @param y la coordonnée verticale à tester.
    * @return true si les coordonnées (x, y) sont contenues dans l'alien, false sinon.
    */
    public boolean contient(int x, int y){
        if ((int) (posX % 4) == 0) {
            if(x >= getPositionX() && x <= getPositionX() + 10 && y >= getPositionY() && y <= getPositionY() + 3) {
                return true;
            }
        }
        else{
            if(x >= getPositionX() && x <= getPositionX() + 10 && y >= getPositionY() && y <= getPositionY() + 5) {
                return true;
            }
        }
        return false;
    }
        
}




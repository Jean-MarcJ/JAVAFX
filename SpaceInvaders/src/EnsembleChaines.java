import java.util.ArrayList;

public class EnsembleChaines  {
    ArrayList<ChainePositionnee> chaines;

    /**
    * Constructeur de la classe EnsembleChaines, qui crée un ensemble vide de chaînes positionnées.
    */
    public EnsembleChaines(){
        chaines= new ArrayList<ChainePositionnee>();
    }

    /**
    * Ajoute une chaîne de caractères positionnée dans l'ensemble.
    * 
    * @param x la position horizontale de la chaîne
    * @param y la position verticale de la chaîne
    * @param c la chaîne de caractères à ajouter
    * @param couleur la couleur de la chaîne de caractères à ajouter
    */
    public void ajouteChaine(int x, int y, String c, String couleur){
        chaines.add(new ChainePositionnee(x,y,c,couleur));}


    /**
    * Ajoute les chaînes d'un autre ensemble de chaînes positionnées à l'ensemble actuel.
    * 
    * @param e l'ensemble de chaînes positionnées à ajouter
    */   
    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            chaines.add(c);
    }

    /**
    * Vérifie si l'ensemble contient une chaîne positionnée à la position (x,y).
    * 
    * @param x la position horizontale à vérifier
    * @param y la position verticale à vérifier
    * @return true si l'ensemble contient une chaîne positionnée à la position (x,y), false sinon
    */
    public boolean contient(int x, int y) {
        for (ChainePositionnee c : chaines) {
            if (c.x == x - 3 && c.y == y ) {
                return true;
            }
        }
        return false;
    }
    
}

public class Vaisseau {

    private double posX;
    private EnsembleChaines ensembleChaines;
    private String color;

    /**
     * Constructeur de la classe Vaisseau.
     * @param posX : position horizontale initiale du vaisseau.
     * @param color : couleur du vaisseau.
     */
    public Vaisseau(double posX, String color) {
        this.posX = posX;
        this.color = color;
    }

    /**
     * Permet de déplacer le vaisseau horizontalement d'une certaine distance.
     * @param dx : distance de déplacement horizontale.
     */
    public void deplace(double dx) {
        this.posX += dx;
    }

    /**
    * Permet d'obtenir l'ensemble de chaînes de caractères représentant le vaisseau.
    * @return EnsembleChaines : ensemble de chaînes de caractères représentant le vaisseau.
    */
    public EnsembleChaines getEnsembleChaines() {
        this.ensembleChaines = new EnsembleChaines();
        this.ensembleChaines.ajouteChaine((int) this.posX, 8, "          ██           ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX, 7, "         ████          ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX, 6, "      ██ ████ ██       ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX, 5, "      ██ ████ ██       ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX,  4," ██   ██████████   ██  ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX,  3," ██ █ ██████████ █ ██  ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX,  2," ████████████████████  ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX,  1,"     █████████████     ",this.color);
        this.ensembleChaines.ajouteChaine((int) this.posX,  0,"        ██ ██ ██       ",this.color);
        return this.ensembleChaines;
    }

    public int getPositionY(){
        int posY = 8;
        return posY;
    }

    /**
    * Permet d'obtenir la position horizontale du canon du vaisseau.
    * @return double : position horizontale du canon du vaisseau.
    */
    public double positionCanon() { 
        return this.posX + 11;
    }

    /**
    * Permet d'obtenir la position horizontale du vaisseau.
    * @return double : position horizontale du vaisseau.
    */
    public double getPositionX(){
        return this.posX;
    }

     /**
     * Permet de déterminer si le vaisseau a été touché par un projectile aux coordonnées données.
     * @param x : un entier représentant la position horizontale
     * @param y : un entier représentant la position verticale
     * @return Un booléen qui est vrai si les coordonnées x et y touchent le vaisseau, faux sinon.
    */
    public boolean touche(int x, int y){
        return getEnsembleChaines().contient(x, y);    
    }

        
    }

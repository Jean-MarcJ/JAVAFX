public class ChainePositionnee{
    int x,y;
    String c;
    private String color;

    /**
    * Constructeur de la classe ChainePositionnee.
    *
    * @param a entier représentant la position X de la chaîne.
    * @param b entier représentant la position Y de la chaîne.
    * @param d chaîne de caractères à positionner.
    * @param couleur chaîne de caractères représentant la couleur de la chaîne.
    */
    
    public ChainePositionnee(int a,int b, String d, String couleur){
        x=a; 
        y=b; 
        c=d; 
        this.color = couleur;
    }

    /**
    * Retourne la taille de la chaîne de caractères.
    *
    * @return la taille de la chaîne de caractères.
    */
    public int tailleChaine(){
        return c.length();
    }

    /**
    * Retourne la couleur de la chaîne de caractères.
    *
    * @return la couleur de la chaîne de caractères.
    */
    public String getColor(){
        return this.color;
    }
    
    
} 

   
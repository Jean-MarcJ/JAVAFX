public class Score {
    private int score;
    
    /**
     * Constructeur de la classe Score. Initialise le score à 0.
     */
    public Score(){
        this.score = 0;
    }

    /**
     * Ajoute une valeur au score.
     * @param valeur La valeur à ajouter.
     */
    public void ajoute(int valeur){
        this.score += valeur;
    }

    /**
     * Renvoie le score actuel.
     * @return Le score actuel.
     */
    public int getScore(){
        return this.score;
    }
}

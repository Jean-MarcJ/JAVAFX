    import javafx.application.Application;
    import javafx.event.EventHandler;
    import javafx.geometry.Insets;
    import javafx.geometry.Pos;
    import javafx.scene.Scene;
    import javafx.scene.control.Button;
    import javafx.scene.image.Image;
    import javafx.scene.image.ImageView;
    import javafx.scene.layout.Pane;
    import javafx.scene.layout.StackPane;
    import javafx.scene.layout.VBox;
    import javafx.scene.paint.Color;
    import javafx.scene.layout.AnchorPane;
    import javafx.scene.layout.Background;
    import javafx.scene.layout.BackgroundImage;
    import javafx.scene.layout.BackgroundPosition;
    import javafx.scene.layout.BackgroundRepeat;
    import javafx.scene.layout.BackgroundSize;
    import javafx.scene.layout.HBox;
    import javafx.stage.Stage;
    import javafx.scene.Group;
    import javafx.scene.text.Text;
    import javafx.scene.text.Font;
    import javafx.scene.text.FontWeight;
    import javafx.animation.Animation;
    import javafx.animation.KeyFrame;
    import javafx.animation.Timeline;
    import javafx.event.ActionEvent;
    import javafx.util.Duration;
    import javafx.scene.input.KeyEvent;
    import javafx.scene.input.KeyCode;
    public class Executable extends Application {
        private Pane root;
        private Group caracteres;
        private GestionJeu gestionnaire;
        private int hauteurTexte;
        private int largeurCaractere;
        private Scene sceneRegle;
        private Scene winnerScene;
        public static void main(String[] args) {
            launch(args);
        }
        private void afficherCaracteres(){
            caracteres.getChildren().clear();
            int hauteur = (int) root.getHeight();
            Text score = (Text) root.lookup("#score");
        
            try {
                score.setText("Score: " + gestionnaire.getScore());
                
            } catch (ExceptionScore e) {
                System.out.println("le score est null");
            }
            for( ChainePositionnee c : gestionnaire.getChaines().chaines)
            {
                Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
                t.setFont(Font.font ("Monospaced", 10));
                caracteres.getChildren().add(t);
            }
        }

        private void lancerAnimation(boolean lancer) {
            Timeline timeline = new Timeline(
                    new KeyFrame(Duration.seconds(0),
                        new EventHandler<ActionEvent>() {
                            @Override public void handle(ActionEvent actionEvent) {
                                gestionnaire.jouerUnTour();
                                afficherCaracteres();
                            }
                        }),
                    new KeyFrame(Duration.seconds(0.025))
                    );
            if(lancer){
            timeline.setCycleCount(Animation.INDEFINITE);
            timeline.play();
            }else
                timeline.stop();       
        }

        @Override
        public void start(Stage primaryStage){
            primaryStage.setTitle("IUTO Space Invader");
            caracteres = new Group();
            root= new AnchorPane(caracteres);
            gestionnaire = new GestionJeu();
            Text t=new Text("█");
            t.setFont(Font.font("Monospaced",10));
            hauteurTexte =(int) t.getLayoutBounds().getHeight();
            largeurCaractere = (int) t.getLayoutBounds().getWidth();
            
            
            Scene sceneNiveau = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            Button button = new Button("Voir les regles");
            button.setOnAction(e -> {
                primaryStage.setScene(sceneRegle);
            });
            Image imageTitre = new Image("file:img/spaceInvaders.png");
            ImageView imageView = new ImageView(imageTitre);
            imageView.setTranslateY(-30);
        
            Image imageBackground = new Image("file:img/background.jpg");
            BackgroundImage backgroundImage = new BackgroundImage(
                imageBackground,
                BackgroundRepeat.REPEAT,
                BackgroundRepeat.REPEAT,
                BackgroundPosition.CENTER,
                BackgroundSize.DEFAULT
            );
            Background background = new Background(backgroundImage);
            
            VBox layout= new VBox(imageView);
            layout.getChildren().addAll(button);
            layout.setBackground(background);
            layout.setAlignment(Pos.CENTER);
            Scene sceneIntro = new Scene(layout, gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            VBox infoNiveau1 = new VBox();
            Text texteNiveau =new Text("Essayer de survivez");
            Text textLancement =new Text("Appuyer sur entrer pour lancer le jeu");
            infoNiveau1.setStyle("-fx-background-color: black;");
            textLancement.setFont(Font.font("Monospaced", 20));
            texteNiveau.setFont(Font.font("Monospaced",25));
            textLancement.setFill(Color.GREEN);
            texteNiveau.setFill(Color.GREEN);
            
            VBox vboxtexte = new VBox(textLancement);
            HBox hboxTexte = new HBox(texteNiveau);
            vboxtexte.setAlignment(Pos.TOP_CENTER);
            hboxTexte.setAlignment(Pos.CENTER);
            infoNiveau1.getChildren().add(texteNiveau);
            infoNiveau1.getChildren().add(textLancement);
            infoNiveau1.setAlignment(Pos.CENTER);
            vboxtexte.setPadding(new Insets(50));
            hboxTexte.setPadding(new Insets(50));
            sceneRegle = new Scene(infoNiveau1, gestionnaire.getLargeur() * largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            
                
            Text winnText = new Text("W I N N E R");
            winnText.setFont(Font.font("Arial", FontWeight.BOLD, 80));
            winnText.setFill(Color.GREEN);
            Text escape = new Text("Appuyer sur Echap pour revenir a l'ecran d'acceuil");
            escape.setFont(Font.font("Arial", FontWeight.BOLD, 10));
            escape.setFill(Color.YELLOW);
            VBox root4 = new VBox(winnText, escape);
            root4.setStyle("-fx-background-color: black;");
            root4.setAlignment(Pos.CENTER);
            root4.setSpacing(10);
            winnerScene = new Scene(root4, gestionnaire.getLargeur() * largeurCaractere, gestionnaire.getHauteur() * hauteurTexte);
        

            Text gameOverText = new Text("GAME OVER");
            gameOverText.setFont(Font.font("Arial", FontWeight.BOLD, 80));
            gameOverText.setFill(Color.RED);
            StackPane root3 = new StackPane(gameOverText);
            root3.setAlignment(Pos.CENTER);
            Scene gameOverScene = new Scene(root3, gestionnaire.getLargeur() * largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            
            sceneRegle.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }
                if(key.getCode() == KeyCode.ENTER){
                    primaryStage.setScene(sceneNiveau);
                    this.gestionnaire = new GestionJeu();
                    this.gestionnaire.Niveau();
                    this.gestionnaire.jouerUnTour();
                    lancerAnimation(true);
                }
            });   
            winnerScene.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }               
            });   

              
            gameOverScene.addEventHandler(KeyEvent.KEY_PRESSED, (key) ->{
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }               
            });   
                
            sceneNiveau.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                if(key.getCode()==KeyCode.LEFT)
                    gestionnaire.toucheGauche();
                if(key.getCode()==KeyCode.RIGHT)
                    gestionnaire.toucheDroite();
                if(key.getCode()==KeyCode.SPACE){
                    gestionnaire.toucheEspace();
                }
                if(key.getCode()==KeyCode.ESCAPE){
                    primaryStage.setScene(sceneIntro);
                }
                if(gestionnaire.estPerdu()){
                    primaryStage.setScene(gameOverScene);  
                }       
                if(gestionnaire.finNiveau()){
                    primaryStage.setScene(winnerScene); 
                }    
                
            });
            primaryStage.setScene(sceneIntro);
            primaryStage.setResizable(false);
            primaryStage.show();
            try {
                Text score;
                score = new Text("Score: " + gestionnaire.getScore());
                score.setId("score");
                score.setFont(Font.font("Monospaced", 20));
                score.setFill(Color.GREEN);
                AnchorPane.setTopAnchor(score, 10.0);
                AnchorPane.setLeftAnchor(score, 10.0);
                root.getChildren().add(score);
            } catch (ExceptionScore e1) {
                System.out.println("n'affiche pas le score");
        
            }    
        }
        
    }

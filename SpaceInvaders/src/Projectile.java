public class Projectile {
    private double posX, posY;
    private String color;


    /**
     * Constructeur de la classe Projectile.
     * 
     * @param posX la position horizontale initiale du projectile.
     * @param posY la position verticale initiale du projectile.
     * @param color la couleur du projectile.
     */
    public Projectile(double posX, double posY, String color){
        this.posX = posX;
        this.posY = posY;
        this.color = color;
    }


    /**
     * Méthode renvoyant un ensemble de chaînes de caractères représentant le projectile.
     * 
     * @return un objet EnsembleChaines représentant le projectile.
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine((int) posX, (int) posY,"°",this.color);
        return chaines;
    }

    
    /**
     * Méthode faisant évoluer la position verticale du projectile.
     */
    public void evolue(){
        this.posY += 0.2;
    }


    /**
     * Méthode renvoyant la position horizontale arrondie du projectile.
     * 
     * @return la position horizontale arrondie du projectile.
     */
    public int getPositionX() {
        return (int) this.posX;
    }

    /**
     * Méthode renvoyant la position verticale arrondie du projectile.
     * 
     * @return la position verticale arrondie du projectile.
     */
    public int getPositionY() {
        return (int) this.posY;
    }
}
